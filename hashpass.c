#include <stdio.h>
#include "md5.h"
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{

    FILE *source_file = fopen(argv[1], "r");
    if (!source_file){
      printf("Can't open %s for writing\n", argv[1]);
      exit(1);  
    }
    
    FILE *h = fopen(argv[2], "w");
    if (!h)
    {
        printf("Can't open %s for writing\n", argv[2]);
        exit(1);
    }
    
    char line[100], *hash;
    while(fgets(line, 100, source_file) != NULL){
        hash = md5(line, (strlen(line)-1));
        fprintf(h, "%s\n", hash);
    }
    
    fclose(h);
    fclose(source_file);

}